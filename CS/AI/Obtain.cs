using System;
using System.Collections.Generic;
using Wintellect.PowerCollections;
using XRL.Rules;
using XRL.World;
using XRL.Core;
using Genkit;
using XRL.World.AI.Pathfinding;
using XRL.World.AI;
using XRL.World.AI.GoalHandlers;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	[Serializable]
	public class Obtain : GoalHandler
	{
		public GameObject Target { get; set; }
		public bool Silent { get; set; }
		public int Energy { get; set; }

		public Obtain(GameObject obj, bool silent = false, int energy = -1) {
			Target = obj;
			Silent = silent;
			Energy = energy;
		}

		public override bool Finished() {
			return Target?.CurrentCell == null;
		}

		public override void Create() {
			Target?.Reserve(ParentObject.DistanceTo(Target) + 5L);
		}

		public override void TakeAction() {
			var cell = Target.CurrentCell;
			if (ParentObject.CurrentCell != cell) {
				if (cell.HasCombatObject()) {
					FailToParent();
				} else {
					PushChildGoal(new MoveTo(cell));
				}
			} else {
				ParentObject.Take(Target, Silent, Energy);
			}
		}
	}
}
