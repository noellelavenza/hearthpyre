using System;
using System.Linq;
using System.Collections.Generic;
using Wintellect.PowerCollections;
using XRL.Rules;
using XRL.World;
using XRL.Core;
using Genkit;
using XRL.World.AI.Pathfinding;
using XRL.World.AI.GoalHandlers;
using static Hearthpyre.Static;

namespace Hearthpyre.AI
{
	// TODO: New MoveTo which caches a pathing solution but only pushes one step at a time.
	public class Travel : IMovementGoal
	{
		public string Destination;
		public int X;
		public int Y;
		public bool Teleport;

		public Travel() { }

		public Travel(string zoneId, int x = -1, int y = -1, bool teleport = false) : this() {
			Destination = zoneId;
			X = x;
			Y = y;
			Teleport = teleport;
		}

		public override bool Finished() {
			return CurrentZone.ZoneID == Destination && X + Y < 0;
		}

		public override void TakeAction() {
			if (MoveToCell()) return;

			if (Teleport) {
				PushTeleport();
				if (!CurrentCell.IsVisible()) return; // Just teleport straight away if not visible.
			}

			var dest = WorldLocation(Destination);
			var curr = WorldLocation(CurrentZone.ZoneID);
			var Z = CurrentZone;
			var wX = dest[0] - curr[0];
			var wY = dest[1] - curr[1];

			GameObject stairs; string step;
			if (TryGetStairs(dest[2], out stairs, out step)) {
				if (!Teleport) PushChildGoal(new Step(step, allowUnbuilt: true));
				PushChildGoal(new MoveTo(stairs));
				return;
			}

			Cell goal = null;
			if (Math.Abs(wX) > Math.Abs(wY)) {
				var off = wX >= 0 ? 79 : 0;
				step = wX >= 0 ? "E" : "W";

				for (int i = 0; i < Z.Height; i++) {
					var cell = Z.GetCell(off, i);
					if (!cell.IsEmpty() || !cell.IsReachable()) continue;

					if (goal == null || Math.Abs(CurrentCell.Y - cell.Y) < Math.Abs(CurrentCell.Y - goal.Y)) {
						goal = cell;
					}
				}
			} else {
				var off = wY >= 0 ? 24 : 0;
				step = wY >= 0 ? "S" : "N";

				for (int i = 0; i < Z.Width; i++) {
					var cell = Z.GetCell(i, off);
					if (!cell.IsEmpty() || !cell.IsReachable()) continue;

					if (goal == null || Math.Abs(CurrentCell.X - cell.X) < Math.Abs(CurrentCell.X - goal.X)) {
						goal = cell;
					}
				}
			}

			if (goal != null) {
				if (!Teleport) PushChildGoal(new Step(step, allowUnbuilt: true));
				PushChildGoal(new MoveTo(goal));
			} else if (CurrentCell.IsVisible()) {
				PushChildGoal(new WanderRandomly(10));
			} else if (!Teleport) {
				PushTeleport();
			}
		}

		bool MoveToCell() {
			if (CurrentZone.ZoneID != Destination) return false;

			var cell = CurrentZone.GetCell(X, Y);
			while (cell != null && cell != CurrentCell) {
				if (!cell.IsReachable() || cell.IsOccludingFor(ParentObject)) {
					cell = cell.LazySpiral(12).FirstOrDefault(x => x.IsReachable() && !cell.IsOccludingFor(ParentObject));
				} else {
					PushChildGoal(new MoveTo(cell));
					return true;
				}
			}
			Pop();
			return true;
		}

		void PushTeleport() {
			var teleport = new DelegateGoal(x => {
				GameManager.Instance.gameQueue.queueTask(() => {
					var zone = GAME.ZoneManager.GetZone(Destination);
					CurrentCell.RemoveObject(ParentObject);
					zone.GetEmptyReachableCells().GetRandomElement().AddObject(ParentObject);
					//PLAYER.TeleportTo(zone.GetEmptyReachableCells().GetRandomElement());
				});
				x.Pop();
			});
			PushChildGoal(teleport);
		}

		bool TryGetStairs(int Z, out GameObject stairs, out string step) {
			if (CurrentZone.Z > Z) {
				stairs = CurrentZone.FindClosestObjectWithPart(ParentObject, "StairsUp", false);
				step = "U";
			} else if (CurrentZone.Z < Z) {
				stairs = CurrentZone.FindClosestObjectWithPart(ParentObject, "StairsDown", false);
				step = "D";
			} else {
				stairs = null;
				step = null;
			}

			return stairs != null;
		}
	}
}
