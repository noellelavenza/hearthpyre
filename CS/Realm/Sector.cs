using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.Rules;
using Hearthpyre;
using System.Linq;
using static Hearthpyre.Static;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// A sector, spans a single zone/screen within the settlement.
	/// </summary>
	public class Sector
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public Dictionary<Guid, Home> Homes { get; } = new Dictionary<Guid, Home>();
		public SectorLocation Location { get; } = new SectorLocation();
		public bool Prime { get; set; }
		public Settlement Settlement { get; set; }

		public string Name => GAME.ZoneManager.GetZoneBaseDisplayName(ZoneID);

		string zoneId;
		public string ZoneID {
			get {
				return zoneId;
			}
			set {
				if (zoneId != null) {
					RealmCache.SectorsByZoneID.Remove(zoneId);
					Settlement.SectorsByZoneID.Remove(zoneId);
				}
				RealmCache.SectorsByZoneID[value] = this;
				Settlement.SectorsByZoneID[value] = this;
				Location.Set(value);

				zone = null;
				commons = null;
				zoneId = value;
			}
		}

		Zone zone;
		public Zone Zone {
			get {
				if (zone != null && zone.bSuspended) Flush();
				
				if (zone == null && ZoneID != null)
					zone = GAME.ZoneManager.GetZone(ZoneID);

				return zone;
			}
			set {
				zone = value;
				commons = null;
				ZoneID = value?.ZoneID;
			}
		}

		HashSet<Cell> commons;
		public HashSet<Cell> Commons {
			get {
				if (zone != null && zone.bSuspended) Flush();
				
				if (commons == null) {
					commons = new HashSet<Cell>(Zone.GetCells());
					foreach (var home in Homes.Values) {
						commons.ExceptWith(home.Cells);
					}
				}
				return commons;
			}
		}


		public Sector(Settlement village, SerializationReader reader) : this(village, null, reader) { }
		public Sector(Settlement village, Zone zone = null, SerializationReader reader = null) {
			Settlement = village;
			if (zone != null) Zone = zone;
			if (reader != null) Load(reader);

			RealmCache.Sector[ID] = this;
		}

		public void Save(SerializationWriter Writer) {
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			fields.Add("Zone", ZoneID);
			fields.Add("Prime", Prime);

			Writer.Write(fields);

			Writer.Write(Homes.Count);
			foreach (var home in Homes.Values) {
				home.Save(Writer);
			}
		}

		public Sector Load(SerializationReader reader) {
			var fields = reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid)val;
			if (fields.TryGetValue("Zone", out val)) ZoneID = (string)val;
			if (fields.TryGetValue("Prime", out val)) Prime = (bool)val;

			for (int i = 0, c = reader.ReadInt32(); i < c; i++) {
				var home = new Home(this, reader);
				Homes[home.ID] = home;
			}

			return this;
		}

		public Home NewHome(Cell C) {
			var home = new Home(this, C);
			var cells = C.LazyCardinalFlood(x => !x.IsStructure());
			if (cells.IsOutside()) cells = cells.Take(5);

			foreach (var cell in cells) {
				home.Add(cell);
			}

			Homes[home.ID] = home;
			return home;
		}

		public void RemoveHome(Home home) {
			RemoveHome(home.ID);
		}
		public void RemoveHome(Guid id) {
			Home home = Homes[id];
			Homes.Remove(id);
			home.Sector = null;
			RealmCache.Homes.Remove(id);
		}

		public void Flush() {
			zone = null;
			commons = null;
			foreach (var home in Homes.Values) {
				home.Flush();
			}
		}
	}
	public class SectorLocation
	{
		public int X { get; set; }
		public int Y { get; set; }
		public int Z { get; set; }

		public void Set(string zoneId) {
			var arr = Static.WorldLocation(zoneId);
			Set(arr[0], arr[1], arr[2]);
		}

		public void Set(int x, int y, int z) {
			X = x;
			Y = y;
			Z = z;
		}

		public int Distance(SectorLocation goal) {
			return Math.Abs(X - goal.X) + Math.Abs(Y - goal.Y) + Math.Abs(Z - goal.Z);
		}
	}
}
