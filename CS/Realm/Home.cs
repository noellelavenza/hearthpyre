using System;
using System.Collections.Generic;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.Rules;
using Hearthpyre;
using System.Linq;
using Genkit;

namespace Hearthpyre.Realm
{
	/// <summary>
	/// An NPC home, spanning several cells within a sector.
	/// </summary>
	public class Home
	{
		public Guid ID { get; private set; } = Guid.NewGuid();
		public Sector Sector { get; set; }

		int[] originXY; Cell origin;
		public Cell Origin {
			get {
				if (origin != null && origin.ParentZone.bSuspended)
					Sector.Flush();

				if (origin == null && originXY != null)
					origin = Sector.Zone.GetCell(originXY[0], originXY[1]);

				return origin;
			}
			set {
				origin = value;
				originXY = new[] { value.X, value.Y };
			}
		}

		HashSet<Location2D> locations; HashSet<Cell> cells;
		public HashSet<Cell> Cells {
			get {
				if (origin != null && origin.ParentZone.bSuspended)
					Sector.Flush();

				if (cells == null) {
					cells = new HashSet<Cell>();
					if (locations != null) {
						var Z = Sector.Zone;
						foreach (var l in locations) {
							cells.Add(Z.GetCell(l));
						}
					}
				}

				return cells;
			}
		}

		public Home(Sector region, SerializationReader reader) : this(region, null, reader) { }
		public Home(Sector region, Cell origin = null, SerializationReader reader = null) {
			Sector = region;
			if (origin != null) Origin = origin;
			if (reader != null) Load(reader);

			RealmCache.Homes[ID] = this;
		}

		public void Save(SerializationWriter Writer) {
			var fields = new Dictionary<string, object>();
			fields.Add("ID", ID);
			fields.Add("Origin", originXY);

			var i = 0;
			var cells = new int[locations.Count][];
			foreach (var l in locations) {
				cells[i++] = new[] { l.x, l.y };
			}

			fields.Add("Cells", cells);

			Writer.Write(fields);
		}

		public Home Load(SerializationReader reader) {
			var fields = reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("ID", out val)) ID = (Guid) val;
			if (fields.TryGetValue("Origin", out val)) originXY = (int[]) val;
			if (fields.TryGetValue("Cells", out val)) {
				var cells = (int[][]) val;
				locations = new HashSet<Location2D>();
				foreach (var cell in cells) {
					locations.Add(Location2D.get(cell[0], cell[1]));
				}
			}

			return this;
		}

		public void Add(IEnumerable<Cell> other) {
			foreach (var cell in other) Add(cell);
		}

		public bool Add(Cell C) {
			if (locations == null) locations = new HashSet<Location2D>();
			Cells.Add(C);
			return locations.Add(C.location);
		}

		public void Remove(IEnumerable<Cell> other) {
			foreach (var cell in other) Remove(cell);
		}

		public bool Remove(Cell C) {
			if (locations == null) locations = new HashSet<Location2D>();
			Cells.Remove(C);
			return locations.Remove(C.location);
		}


		public void Flush() {
			origin = null;
			cells = null;
		}
	}
}