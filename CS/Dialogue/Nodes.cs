using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using XRL;
using XRL.Rules;
using XRL.Core;
using XRL.Language;
using XRL.World;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.World.AI.GoalHandlers;
using Hearthpyre.AI;
using Genkit;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue
{
	/// <summary>
	/// Localised conversation node, alters text based on speaker.
	/// </summary>
	[Serializable]
	public class DialogueNode : ConversationNode
	{
		// Instantiate with StringComparer.OrdinalIgnoreCase.
		public Dictionary<string, string[]> Passages;

		public DialogueNode() {
			alwaysShowAsNotVisted = true;
			base.OnLeaveNode = OnLeaveNode;
		}

		/// <summary>
		/// Fill the Text field with a random text localised to the speaker.
		/// </summary>
		public override void Visit(GameObject speaker, GameObject player) {
			base.Visit(speaker, player);
			Text = GetText(speaker).GetRandomElement();
			Text = VariableReplace(speaker).ToString();
		}

		public new void OnLeaveNode() {
			foreach (var choice in Choices.OfType<DialogueChoice>()) {
				choice.Text = string.Empty;
			}
		}

		///	<summary>
		/// Try to find specialized conversation text for this speaker.
		/// Snapjaws snarling, insects clicking. Inherits choices from default if none intrinsic.
		/// </summary>
		/// <returns>First encountered text from conversation id, blueprint, species, faction or inheritance tree.</returns>
		// TODO: Split off into separate dictionaries to avoid concat?
		// TODO: Reverse lookup? Build blueprint lookup cache once on boot? <-- this probably
		string[] GetText(GameObject speaker) {
			string id; string[] passages;

			id = speaker.GetPart<ConversationScript>()?.ConversationID;
			if (id != null && Passages.TryGetValue("Conversation." + id, out passages))
				return passages;

			id = speaker.Blueprint;
			if (id != null && Passages.TryGetValue("Blueprint." + id, out passages))
				return passages;

			id = speaker.GetTagOrStringProperty("Species");
			if (id != null && Passages.TryGetValue("Species." + id, out passages))
				return passages;

			id = speaker.pBrain?.GetPrimaryFaction();
			if (id != null && Passages.TryGetValue("Faction." + id, out passages))
				return passages;

			// HACK: Check the Humanoid int property directly.
			if (speaker.GetIntProperty("Humanoid") >= 1 && Passages.TryGetValue("Property.Humanoid", out passages))
				return passages;

			id = speaker.GetBlueprint().Inherits;
			while (id != null) {
				if (Passages.TryGetValue("Blueprint." + id, out passages))
					return passages;

				GameObjectBlueprint blueprint;
				GameObjectFactory.Factory.Blueprints.TryGetValue(id, out blueprint); // Dodge log spam of GetBlueprint.
				id = blueprint?.Inherits;
			}

			return Passages["Default"];
		}

		public virtual StringBuilder VariableReplace(GameObject speaker) {
			var text = new StringBuilder(Text);

			var index = text.IndexOf("=onomatopoeia=");
			if (index > -1) {
				text.Remove(index, 14);
				text.Insert(index, GetOnomatopoeia(speaker));
			}

			index = text.IndexOf("=anatomy.");
			if (index > -1) {
				var sb = Strings.SB.Clear();
				var cap = true;
				for (int i = index + 9, c = text.Length; i < c; i++) {
					var chr = text[i];
					if (chr == '=') break;

					if (cap) {
						sb.Append(char.ToUpper(chr));
						cap = false;
					} else {
						if (char.IsWhiteSpace(chr)) cap = true;
						sb.Append(chr);
					}
				}

				var str = sb.ToString();
				var name = speaker.GetPart<Body>()?.GetFirstPart(str, true)?.Name;
				if (String.IsNullOrEmpty(name)) name = str.ToLower();

				text.Remove(index, sb.Length + 10);
				text.Insert(index, name);
			}

			return text;
		}

		// Unsure whether to give ConversationScript or SimpleConversation priority here.
		// Hmm, turns out ConversationScript since base Creature has a SimpleConversation we'd rather not see.
		private string GetOnomatopoeia(GameObject speaker, string alt = "{{w|*soft growling*}}") {
			var id = speaker.GetPart<ConversationScript>()?.ConversationID;
			if (id != null) {
				var conversations = ConversationLoader.Loader.ConversationsByID;

				Conversation chat;
				if (conversations.TryGetValue(id, out chat) && chat.StartNodes.Any()) {
					return chat.StartNodes[0].Text.Split('~').GetRandomElement();
				}
			}

			var simple = speaker.GetTag("SimpleConversation");
			if (simple != null) {
				return simple.Split('~').GetRandomElement();
			}

			return alt;
		}

		public ConversationChoice AddChoice(ConversationChoice choice) {
			choice.ParentNode = this;
			Choices.Add(choice);
			return choice;
		}
	}

	/// <summary>
	/// Populates choices with player settlements.
	/// </summary>
	[Serializable]
	public class SettlementChoiceNode : DialogueNode
	{
		public override void Visit(GameObject speaker, GameObject player) {
			base.Visit(speaker, player);

			var governor = player.GetPart<HearthpyreGoverning>();
			var cost = -CalculateRepCost(speaker);
			var end = Choices.First(x => x.GotoID != "InviteAccept");
			Choices.Clear();
			//foreach (var village in governor.Villages.Values) {
			foreach (var pair in governor.Settlements) {
				var settlement = pair.Value;
				var directions = LoreGenerator.GenerateLandmarkDirectionsTo(settlement.Prime.ZoneID);
				AddChoice(new DialogueChoice {
					Text = $"{settlement.Name}. &g[&C{cost}&g reputation]&G\n{directions}.",
					GotoID = "InviteAccept",
					onAction = () => {
						var settler = speaker.RequirePart<HearthpyreSettler>();
						settler.Settlement = settlement;
						GAME.PlayerReputation.modify(speaker.pBrain.GetPrimaryFaction(), cost, true);
						return true;
					}
				});
			};
			AddChoice(end);
		}

		public int CalculateRepCost(GameObject speaker) {
			double result = 100;

			Statistic level;
			if (speaker.Statistics.TryGetValue("Level", out level))
				result = level.Value * 2.5d;

			if (speaker.GetIntProperty("Hero") >= 1)
				result *= 1.25;

			if (PLAYER.HasSkill("Customs_Tactful"))
				result *= 0.75;

			return Calc.Clamp((int)(Math.Round(result / 5d) * 5d), 25, 250);
		}
	}
}
