using System;
using System.Reflection;
using System.Collections;
using System.Collections.Generic;
using XRL;
using XRL.UI;
using XRL.Core;
using XRL.World;
using LitJson;
using System.IO;
using System.Linq;
using static Hearthpyre.Static;

namespace Hearthpyre.Dialogue
{
	[HasModSensitiveStaticCache]
	public static class DialogueLoader
	{
		// Store our custom dialogues here since've no need of serialising 'em.
		[ModSensitiveStaticCache(true)]
		public static Dictionary<string, Dialogue> Dialogues;
		public static StringComparer Comparer = StringComparer.OrdinalIgnoreCase;

		[ModSensitiveCacheInit]
		public static void Reset() {
			Loading.LoadTask("Loading Dialogue.json", Init);
		}

		public static void Init() {
			ModManager.ForEachFile("Dialogue.json", (path, mod) => {
				if (!mod.IsApproved) return;
				Log($"Loading Dialogue.json from {mod.ID}...");

				using (var reader = File.OpenText(path)) {
					var json = JsonMapper.ToObject(reader);
					foreach (KeyValuePair<string, JsonData> dialogue in json) {
						try { Dialogues[dialogue.Key] = ReadDialogue(dialogue.Key, dialogue.Value); } catch (Exception e) {
							Log($"Error loading dialogue {dialogue.Key} from mod {mod.ID}", e);
						}
					}
				}
			});
		}

		static Dialogue ReadDialogue(string id, JsonData data) {
			var dialogue = new Dialogue { ID = id };
			foreach (KeyValuePair<string, JsonData> node in data) {
				try { dialogue.AddNode(ReadNode(node.Key, node.Value)); } catch (Exception e) {
					Log($"Error loading dialogue node {node.Key} ", e);
				}
			}
			return dialogue;
		}

		static DialogueNode ReadNode(string id, JsonData data) {
			IDictionary dict = data;
			var node = ResolveNode(data);
			node.ID = id;

			ReadValues(node, data, "Class", "Text", "Choices");

			if (dict.Contains("Text")) {
				var text = data["Text"];
				node.Passages = ReadStringArrayObject(text);
			}

			if (dict.Contains("Choices")) {
				foreach (JsonData choice in data["Choices"]) {
					try { node.AddChoice(ReadChoice(choice)); } catch (Exception e) {
						Log($"Error loading choice node", e);
					}
				}
			}

			return node;
		}

		static DialogueChoice ReadChoice(JsonData data) {
			IDictionary dict = data;
			var choice = ResolveChoice(data);

			ReadValues(choice, data, "Class", "Text");

			if (dict.Contains("Text")) {
				var text = data["Text"];
				choice.Passages = ReadStringArray(text);
			}

			return choice;
		}

		/// <summary>
		/// Read JSON string array or string.
		/// </summary>
		static string[] ReadStringArray(JsonData data) {
			if (data.GetJsonType() == JsonType.String)
				return new[] { (string)data };

			var arr = new string[data.Count];
			for (int i = 0; i < arr.Length; i++)
				arr[i] = (string)data[i]; // Generic Linq array cast throws.
			return arr;
		}

		/// <summary>
		/// Read JSON string array object, string array, or string.
		/// </summary>
		static Dictionary<string, string[]> ReadStringArrayObject(JsonData data) {
			var dict = new Dictionary<string, string[]>(Comparer);

			if (data.GetJsonType() != JsonType.Object) {
				dict["Default"] = ReadStringArray(data);
				return dict;
			}

			foreach (KeyValuePair<string, JsonData> pair in data) {
				dict[pair.Key] = ReadStringArray(pair.Value);
			}

			return dict;
		}

		/// <summary>
		/// Read basic value fields into object. 
		/// </summary>
		/// <param name="target">Dialogue node or choice.</param>
		/// <param name="data">JSON data node.</param>
		/// <param name="excluded">JSON fields to ignore, e.g. Dialogue text and Node/Choice class.</param>
		// TODO: Recurse into objects and arrays.
		static void ReadValues(object target, JsonData data, params string[] excluded) {
			var type = target.GetType();

			foreach (KeyValuePair<string, JsonData> pair in data) {
				try {
					if (excluded.Contains(pair.Key)) continue;

					var jsonType = pair.Value.GetJsonType();
					var field = type.GetField(pair.Key, BindingFlags.Public | BindingFlags.Instance);
					if (field == null) continue;

					switch (jsonType) {
						case JsonType.Boolean:
							field.SetValue(target, (bool)pair.Value);
							break;
						case JsonType.Double:
							var d = (double)pair.Value;
							if (field.FieldType == typeof(double))
								field.SetValue(target, d);
							else if (field.FieldType == typeof(float))
								field.SetValue(target, (float)d);
							break;
						case JsonType.Int:
							var i = (int)pair.Value;
							if (field.FieldType == typeof(int))
								field.SetValue(target, i);
							else if (field.FieldType == typeof(long))
								field.SetValue(target, (long)i);
							break;
						case JsonType.Long:
							var l = (long)pair.Value;
							if (field.FieldType == typeof(long))
								field.SetValue(target, l);
							else if (field.FieldType == typeof(int))
								field.SetValue(target, (int)l);
							break;
						case JsonType.String:
							field.SetValue(target, (string)pair.Value);
							break;
					}
				} catch (Exception e) {
					throw new Exception("Invalid data for: " + pair.Key, e);
				}
			}
		}

		static DialogueNode ResolveNode(IDictionary data) {
			if (data.Contains("Class")) {
				var type = ModManager.ResolveType((string)(JsonData)data["Class"]);
				return Activator.CreateInstance(type) as DialogueNode;
			}

			return new DialogueNode();
		}

		static DialogueChoice ResolveChoice(IDictionary data) {
			if (data.Contains("Class")) {
				var type = ModManager.ResolveType((string)(JsonData)data["Class"]);
				return Activator.CreateInstance(type) as DialogueChoice;
			}

			return new DialogueChoice();
		}
	}
}
