using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using XRL.Core;
using XRL.Language;
using XRL.UI;
using XRL.World.Capabilities;
using XRL.World.Skills;
using XRL.World.Tinkering;
using XRL.Rules;
using System.Reflection;
using Hearthpyre;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	[Serializable]
	public class HearthpyreSchematic : IPart
	{
		public const string INV_CMD = "InsertSchematicDrive";

		public string Package { get; set; }
		int Unlocked = -1;
		int FrameOffset;

		public override void SaveData(SerializationWriter Writer) {
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields["Package"] = Package;
			Writer.Write(fields);
		}

		public override void LoadData(SerializationReader Reader) {
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("Package", out val)) Package = (string)val;
		}

		public override bool SameAs(IPart p) {
			var book = p as HearthpyreSchematic;
			return book != null && Package == book.Package;
		}

		public override bool CanGenerateStacked() {
			return false;
		}

		public override bool Render(RenderEvent E) {
			var f = (XRLCore.CurrentFrame + FrameOffset) & 511;
			if (f < 8)
				E.DetailColor = "Y";
			else if (f < 16)
				E.ColorString = "&Y";

			FrameOffset += Stat.RandomCosmetic(0, 24);
			if (Stat.RandomCosmetic(1, 400) == 1) {
				E.DetailColor = "Y";
				E.ColorString = "&Y";
			}
			return true;
		}

		public override bool WantEvent(int ID, int cascade) {
			return
				base.WantEvent(ID, cascade)
				|| ID == GetInventoryActionsEvent.ID
				|| ID == GetShortDescriptionEvent.ID
				|| ID == InventoryActionEvent.ID
				|| ID == ObjectCreatedEvent.ID
				|| ID == GetDisplayNameEvent.ID
			;
		}

		public override bool HandleEvent(GetDisplayNameEvent E) {
			if (!ParentObject.Understood()) return true;
			if (String.IsNullOrEmpty(Package)) return true;

			E.AddTag("{{y|[{{W|" + Package + "}}]}}");
			return true;
		}

		public override bool HandleEvent(GetShortDescriptionEvent E) {
			if (!ParentObject.Understood()) return true;
			if (Unlocked == -1) {
				Unlocked = Notitia.Categories.SelectMany(x => x.Value.AllBlueprints)
					.Where(x => x.Package == Package)
					.Count();
			}

			E.Postfix.Append("\n{{y|Contains {{W|").Append(Unlocked).Append("}} blueprints.}}");
			return true;
		}

		public override bool HandleEvent(GetInventoryActionsEvent E) {
			if (!ParentObject.Understood()) return true;

			E.AddAction(Name: "Upgrade", Display: "upg{{W|r}}ade", Command: INV_CMD, Key: 'r');
			return true;
		}

		public override bool HandleEvent(InventoryActionEvent E) {
			if (E.Command != INV_CMD) return true;
			if (!ParentObject.Understood()) return true;

			var schemers = E.Actor.LazyForeachItems(x => x.Blueprint == OBJ_SCMR).ToList();
			if (schemers.Count == 0) {
				var sample = GameObjectFactory.Factory.CreateSampleObject(OBJ_SCMR);
				E.Actor.PlayerPopup($"&YYou don't have {sample.a}{sample.DisplayNameOnlyDirect}&Y to upgrade.");
				return true;
			}

			var schemer = schemers[0];
			if (schemers.Count > 1) {
				var options = schemers.Select(SchemeOption).ToArray();
				var i = Popup.ShowOptionList($"&YWhich {schemer.DisplayNameOnlyDirect}&Y do you wish to upgrade?", options);
				if (i < 0) return true;

				schemer = schemers[i];
			}

			var part = schemer.GetPart<HearthpyreXyloschemer>();
			if (part == null) return true;
			if (part.Packages.Contains(Package)) {
				E.Actor.PlayerPopup($"{schemer.The}{schemer.DisplayNameOnly}&Y already has that upgrade.");
				return true;
			}

			part.Packages.Add(Package);
			PlayUISound("compartment_close_whine_up");
			E.Actor.PlayerPopup($"{schemer.The}{schemer.DisplayNameOnly}&Y hums merrily, shifting a verdant green as it draws nourishment from the binary sequences.");
			ParentObject.Destroy();

			return true;
		}

		public string SchemeOption(GameObject obj) {
			var sb = Strings.SB.Clear();
			sb.Append(obj.DisplayName).Append("\n");
			sb.Append(obj.EquippedOn()?.Description ?? "Inventory");

			var part = obj.GetPart<HearthpyreXyloschemer>();
			if (part != null) {
				sb.Append(", ").Append(part.Packages.Count).Append(" upgrade");
				if (part.Packages.Count != 1) sb.Append("s");
			}

			return sb.ToString();
		}
	}
}
