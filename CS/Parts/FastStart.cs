using System;

using XRL;
using XRL.UI;
using XRL.Wish;
using XRL.World;
using XRL.World.Parts;
using static Hearthpyre.Static;

namespace Hearthpyre
{
	[PlayerMutator, HasWishCommand]
	public class FastStart : IPlayerMutator
	{
		[WishCommand(Command = "learned builder")]
		public static void Wish() {
			AddTo(The.Player);
		}

		public void mutate(GameObject player) {
			if (!OptionFastStart) return;

			AddTo(player);
		}

		public static void AddTo(GameObject Subject) {
			try {
				var factory = GameObjectFactory.Factory;
				var obj = factory.CreateObject(OBJ_SCMR, BonusModChance: -9999);
				obj.GetPart<Commerce>().Value = 0;
				obj.GetPart<Examiner>()?.MakeUnderstood();
				var cell = obj.GetPart<EnergyCellSocket>()?.Cell;
				if (cell != null) {
					cell.GetPart<Commerce>().Value = 0;
					cell.GetPart<Examiner>()?.MakeUnderstood();
					cell.GetPartDescendedFrom<IEnergyCell>()?.MaximizeCharge();
				}

				Subject.TakeObject(obj, Silent: true);
				TakeObject(Subject, "Merchant's Token");
				TakeObject(Subject, "HearthpyreTokenWarden");
				TakeObject(Subject, "HearthpyreTokenTinker");
			} catch (Exception e) {
				MetricsManager.LogError("Hearthpyre fast start error", e);
			}
		}

		public static void TakeObject(GameObject Subject, string Blueprint) {
			var obj = GameObjectFactory.Factory.CreateObject(Blueprint, BonusModChance: -9999);
			var comm = obj.GetPart("Commerce") as Commerce;
			if (comm != null) comm.Value = 0;

			Subject.TakeObject(obj, Silent: true);
		}
	}
}
