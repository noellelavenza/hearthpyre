using System;
using XRL.UI;
using System.Collections.Generic;
using Hearthpyre;
using System.Linq;
using static Hearthpyre.Static;

namespace XRL.World.Parts
{
	// TODO: Could make an 8 directional sprite n' have it follow the player when equipped.
	// Bad idea, perhaps? Fuck it, doin' it. Make it solid so backing up is a hassle. Yeah!
	[Serializable]
	public class HearthpyreCart : IPart
	{
		// Colour name here to let blueprint name be easily wishable.
		public const string DSP_NAME = "{{w-y-w-w-w-y-w-w sequence|handcart}}";
		public int Capacity { get; set; }
		public int MoveSpeed { get; set; }

		int MSApplied;
		GameObject Deployed;

		public override bool SameAs(IPart p) {
			return false;
		}

		public override void SaveData(SerializationWriter Writer) {
			base.SaveData(Writer);

			var fields = new Dictionary<string, object>();
			fields.Add("Capacity", Capacity);
			fields.Add("MoveSpeed", MoveSpeed);
			fields.Add("MSApplied", MSApplied);
			Writer.Write(fields);

			Writer.WriteGameObject(Deployed);
		}

		public override void LoadData(SerializationReader Reader) {
			base.LoadData(Reader);

			var fields = Reader.ReadDictionary<string, object>();
			object val;

			if (fields.TryGetValue("Capacity", out val)) Capacity = (int)val;
			if (fields.TryGetValue("MoveSpeed", out val)) MoveSpeed = (int)val;
			if (fields.TryGetValue("MSApplied", out val)) MSApplied = (int)val;

			Deployed = Reader.ReadGameObject();
		}

		#region Events
		public override bool WantEvent(int ID, int cascade) {
			return base.WantEvent(ID, cascade) || ID == EquippedEvent.ID ||
				ID == UnequippedEvent.ID || ID == GetShortDescriptionEvent.ID ||
				ID == GetIntrinsicWeightEvent.ID || ID == ObjectCreatedEvent.ID;
		}

		public override bool HandleEvent(EquippedEvent E) {
			E.Actor.RegisterPartEvent(this, "GetMaxWeight");
			RecalculateStats(E.Actor, null, true);
			DeployCart(E.Actor);
			return true;
		}

		public override bool HandleEvent(UnequippedEvent E) {
			E.Actor.UnregisterPartEvent(this, "GetMaxWeight");
			RecalculateStats(E.Actor);

			if (Deployed != null) {
				Deployed.Destroy("Unequipped");
				Deployed = null;
			}
			return true;
		}

		public override bool HandleEvent(GetShortDescriptionEvent E) {
			var ms = MoveSpeed;
			var obj = ParentObject.Equipped ?? ParentObject.InInventory;
			if (obj != null && obj.HasSkill(GOV_PRT))
				ms = (int)Math.Round(ms * 0.5);

			E.Postfix.Append("\n&C").Append(Statistic.GetStatAdjustDescription("MoveSpeed", ms));
			return true;
		}

		public override bool HandleEvent(GetIntrinsicWeightEvent E) {
			if (E.Object.Equipped != null)
				E.Weight = 0;
			return true;
		}

		public override bool HandleEvent(ObjectCreatedEvent E) {
			E.Object.pRender.DisplayName = DSP_NAME;
			return true;
		}

		public override bool FireEvent(Event E) {
			// TODO: Check for future GetMaxWeight MinEvent.
			if (E.ID == "GetMaxWeight" && ParentObject.Equipped != null)
				E.SetParameter("Weight", E.GetIntParameter("Weight") + Capacity);

			return base.FireEvent(E);
		}
		#endregion

		void RecalculateStats(GameObject obj, BodyPart limb = null, bool apply = false) {
			var porter = obj.HasSkill(GOV_PRT);
			var armor = ParentObject.GetPart<Armor>();
			var blueprint = ParentObject.GetBlueprint();
			var av = Int32.Parse(blueprint.GetPartParameter("Armor", "AV", "-6"));
			var dv = Int32.Parse(blueprint.GetPartParameter("Armor", "DV", "-6"));
			var ms = MoveSpeed;
			if (porter && apply) {
				av = RoundToInt(av * GOV_PRT_MUL);
				dv = RoundToInt(dv * GOV_PRT_MUL);
				ms = RoundToInt(ms * GOV_PRT_MUL);
			}

			armor.AV = av;
			armor.DV = dv;

			var stat = obj.Statistics["MoveSpeed"];
			if (MSApplied != 0) {
				if (MSApplied > 0) stat.Bonus -= MSApplied;
				else stat.Penalty -= MSApplied;
				MSApplied = 0;
			}

			if (apply) {
				armor.RecalculateArmor(obj, limb, true);
				if (ms != 0) {
					if (ms > 0) stat.Bonus += ms;
					else stat.Penalty += ms;
					MSApplied = ms;
				}
			}
		}

		void DeployCart(GameObject obj) {
			if (Deployed != null) return;
			if (!OptionDeployCart) return;

			var cell = obj.CurrentCell.LazySpiral(3).Where(x => x.IsEmpty()).GetRandomElement();
			if (cell == null) return;

			Deployed = cell.AddObject("HearthpyreHandcartDeployed");

			var pullable = Deployed.GetPart<HearthpyreTowed>();
			pullable.SetItem(ParentObject);
			pullable.SetOwner(obj);
		}
	}
}
