using System;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;


using XRL.UI;
using XRL.Core;
using XRL.World;
using XRL.World.Parts;
using XRL.World.WorldBuilders;
using XRL.Language;
using ConsoleLib.Console;
using SimpleJSON;
using Qud.API;
using Hearthpyre.Realm;
using Genkit;
using XRL.World.Parts.Skill;
using static Hearthpyre.Static;
//using Rewired;

namespace Hearthpyre.UI
{
	/// <summary>
	/// Just used as an alternative initialiser because some things're not up with how early ModSensitiveCacheInit runs.
	/// ColorUtility mostly.
	/// </summary>
	[UIView("HearthpyreEmptyView")]
	public class EmptyView : IWantsTextConsoleInit
	{
		public void Init(TextConsole console, ScreenBuffer buffer) {
			Static.LoadColorMap();
			Notitia.Load();
		}
	}
}
