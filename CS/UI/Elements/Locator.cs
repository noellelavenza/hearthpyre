using ConsoleLib.Console;
using System;
using System.Linq;
using XRL.Core;
using XRL.World;
using XRL.World.WorldBuilders;
using XRL.World.Parts;
using XRL.World.Parts.Skill;
using XRL.UI;
using XRL.Language;
using Hearthpyre.Realm;
using Qud.API;
using System.Diagnostics;
using static Hearthpyre.Static;

namespace Hearthpyre.UI
{
	public class Locator : Element
	{
		bool Disabled;
		bool ActiveFrame;

		public event EventHandler Reload;
		public HearthpyreGoverning Governor;
		public ZoneManager Manager;
		public Zone ActiveZone;
		public GameObject ActiveTerrain;
		public Settlement ActiveSettlement;
		public Sector ActiveSector;

		public Locator() { }

		public void Enter() {
			Manager = GAME.ZoneManager;
			Governor = PLAYER.GetPart<HearthpyreGoverning>();

			ActiveZone = GAME.ZoneManager.ActiveZone;
			ActiveTerrain = ActiveZone.GetTerrainObject();
			ActiveSettlement = Governor?.GetSettlement(ActiveZone);
			ActiveSector = ActiveSettlement?.GetSector(ActiveZone);

			Disabled = Governor == null;
		}

		public void Update() {
			ActiveFrame = true;
		}

		public void Render(ScreenBuffer sb) {
			int x = sb.X - 4, y = sb.Y;

			var text = "Claim";
			if (ActiveSettlement != null) {
				if (ActiveSector != null) {
					text = "Label";
				} else {
					text = "Annex";
				}
			}

			if (ActiveFrame) {
				if (Disabled) sb.Write(x, y, "{{K|> " + text + " <}}");
				else sb.Write(x, y, "> {{C|" + text + "}} <");
				ActiveFrame = false;
			} else {
				sb.Write(x, y, "  " + text + "  ");
			}
		}

		public bool Input(Keys keys) {
			if (Disabled) return false;
			switch (keys) {
				case Keys.Space:
				case Keys.Enter:
					Locate();
					break;
				case Keys.H:
					ShowHelp();
					break;
				case Keys.Control | Keys.D:
				case Keys.Shift | Keys.D:
					if (Describe()) PlayUISound(SND_TEXT);
					break;
				default: return false;
			}


			return true;
		}

		void Locate() {
			if (ActiveSettlement != null) {
				if (ActiveSector != null) {
					if (Label()) PlayUISound(SND_TEXT);
				} else {
					if (Annex()) PlayUISound(SND_ADD);
				}
			} else {
				Claim();
			}
		}

		bool Claimable() {
			var location = Zone.zoneIDTo240x72Location(ActiveZone.ZoneID);
			var info = Governor.World.GetGeneratedLocationAt(location);

			if (info != null) {
				if (info.ownerID != Governor.ParentObject.id)
					Popup.Show("This zone is owned by someone else and can't be claimed by you.");
				else
					Popup.Show("You already own this zone.");
				return false;
			}

			// The zone is under an immutable world cell.
			if (Manager.GetCellBlueprints(ActiveZone.ZoneID).Any(b => !b.Mutable)) {
				Popup.Show("This zone is unclaimable.");
				return false;
			}

			if (ActiveTerrain.GetIntProperty("ForceMutableSave") >= 1) {
				Popup.Show("This zone is unclaimable.");
				return false;
			}

			var count = Governor.Settlements.Count;
			var claimer = Governor.ParentObject;

			if (count >= 1 && !claimer.HasSkill(GOV_DST1) ||
				count >= 2 && !claimer.HasSkill(GOV_DST2) ||
				count >= 4 && !claimer.HasSkill(GOV_DST3)) {
				Popup.Show("Your governing skills are at capacity.");
				return false;
			}
			return true;
		}

		void Claim() {
			if (!Claimable()) return;
			if (!Label()) return;

			var name = ActiveZone.BaseDisplayName;
			ActiveSettlement = Governor.NewSettlement(ActiveZone);
			ActiveSector = ActiveSettlement.NewSector(ActiveZone);
			PlayUISound(SND_CLM);

			if (!JournalAPI.GetMapNotesForZone(ActiveZone.ZoneID).Any(x => x.category == "Settlements")) {
				JournalAPI.AddMapNote(ActiveZone.ZoneID, name, "Settlements", null, ActiveSettlement.ID.ToString(), true, true);
			}

			AddMapTile(name);
			Reload.Invoke(this, new EventArgs());
		}

		void AddMapTile(string name) {
			var terrain = ActiveTerrain;
			terrain.SetIntProperty("ForceMutableSave", 1);
			terrain.SetIntProperty("ProperNoun", 1);
			terrain.RemovePart("AnimatedMaterialSaltDunes");
			terrain.RemovePart("AnimatedMaterialOverlandWater");

			var render = terrain.pRender;
			render.Tile = ICO_CLM;
			render.RenderString = "#";
			render.DetailColor = Governor.ParentObject.pRender.DetailColor;
			render.DisplayName = name;

			var color = render.DetailColor;
			while (color == render.DetailColor)
				color = Crayons.GetRandomColorAll();
			render.ColorString = '&' + color;
		}

		bool Label() {
			var name = Popup.AskString("Enter a name for this zone. {{W|ESC}} to cancel.", ActiveZone.BaseDisplayName, 99);
			if (String.IsNullOrEmpty(name)) return false;

			ActiveZone.HasProperName = true;
			ActiveZone.NamedByPlayer = true;
			ActiveZone.IncludeContextInZoneDisplay = true;
			ActiveZone.IncludeStratumInZoneDisplay = false;
			ActiveZone.DisplayName = name;
			if (ActiveSector?.Prime == true) {
				ActiveSettlement.Terrain.pRender.DisplayName = name;
				var note = JournalAPI.GetMapNote(ActiveSettlement.ID.ToString());
				if (note != null) {
					note.text = name;
					note.Updated();
				}
			}

			Reload.Invoke(this, new EventArgs());
			return true;
		}

		bool Annex() {
			ActiveSector = ActiveSettlement.NewSector(ActiveZone);
			Reload.Invoke(this, new EventArgs());
			return true;
		}

		bool Describe() {
			var part = ActiveSettlement?.Terrain?.GetPart<Description>();
			if (part == null) return false;

			var desc = Popup.AskString("Compose a description for this settlement. {{W|ESC}} to cancel.", part._Short, 256);
			if (String.IsNullOrEmpty(desc)) return false;

			part.Short = desc;
			return true;
		}

		public void ShowHelp() {
			var text = MakeshiftPopupTitle("{{W|LOCATE HELP}}") + "\n" +
				"Press the {{W|Enter}} or {{W|Space}} keys to claim, label or annex the current zone.\n\n" +
				"Claiming erects a new settlement, labeling will rename the current zone, and annexing will attach this zone to the existing settlement.\n\n" +
				"Press {{W|Shift}} + {{W|D}} to write a description.";

			Popup.Show(text);
		}

		public void Leave() {
			if (ActiveSector != null) {
				var C = ActiveZone.GetCell(0, 0);
				if (!C.HasObjectWithPart(PRT_SECT))
					C.AddObject(OBJ_SECT);
			}
		}
	}
}
